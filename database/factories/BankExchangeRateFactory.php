<?php

namespace Database\Factories;

use App\Models\BankExchangeRate;
use Illuminate\Database\Eloquent\Factories\Factory;

class BankExchangeRateFactory extends Factory
{
    protected $model = BankExchangeRate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $currency = [ 'EUR', 'USD', 'GBP'];

        return [
            'currency' => $currency[rand(0,2)],
            'date' => $this->faker->date,
            'amount' => $this->faker->randomNumber(2),
            'bank_id' =>  rand(1,10),
        ];
    }
}
