<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankExchangeRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bankExchangeRate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('currency',[
                'EUR', 'USD', 'GBP'
            ]);
            $table->date('date');
            $table->decimal('amount',30,2);
            $table->foreignId('bank_id')->references('id')->on('bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bankExchangeRate');
    }
}
