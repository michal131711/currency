<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\BankExchangeRate;
use Illuminate\Database\Seeder;

class BankExchangeRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BankExchangeRate::factory(10)->create()->each(function ($u) {
            BankExchangeRate::factory(Bank::class)->create(['bank_id' => $u->id]);
        });
    }
}
