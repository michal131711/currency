<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\User;
use Illuminate\Database\Seeder;

class bankDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::factory(11)->create()->each(function ($u) {
            Bank::factory(User::class)->create(['user_id' => $u->id]);
        });
    }
}
