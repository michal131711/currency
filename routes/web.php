<?php

use App\Http\Controllers\Controllers\BankController;
use App\Http\Controllers\Controllers\BankExchangeRateController;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', function (Request $request): JsonResponse {
    try {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->plainTextToken;
            $success['name'] = $user->name;
            return response()->json($success, ResponseAlias::HTTP_CREATED);
        }
    } catch (QueryException $e) {
        dd($e->errorInfo[2]);
    }
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::controller(BankController::class)->group(function () {
        Route::get('banks', 'index')->name('list.banks');
        Route::post('banks', 'store')->name('store.banks');
        Route::get('banks/{id}', 'show')->name('show.banks');
        Route::put('banks/{id}', 'update')->name('update.banks');
        Route::delete('banks/{id}', 'destroy')->name('destroy.banks');
    });

    Route::controller(BankExchangeRateController::class)->group(function () {
        Route::get('banksExchangeRate', 'index')->name('list.banksExchangeRate');
        Route::post('banksExchangeRate', 'store')->name('store.banksExchangeRate');
        Route::get('banksExchangeRate/{id}', 'show')->name('show.banksExchangeRate');
        Route::get('banksExchangeRate/{date}/dates', 'showByDate')->name('showByDate.banksExchangeRate');
        Route::get('banksExchangeRate/{currency}/currency/{date}/dates', 'showByCurrencyWithDate')->name('showByCurrencyWithDate.banksExchangeRate');
        Route::put('banksExchangeRate/{id}', 'update')->name('update.banksExchangeRate');
        Route::delete('banksExchangeRate/{id}', 'destroy')->name('destroy.banksExchangeRate');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
