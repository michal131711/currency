<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BankExchangeRate extends Model
{
    use HasFactory;

    protected $table = 'bankExchangeRate';

    protected $fillable = [
        'bank_id', 'currency', 'date', 'amount'
    ];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function belongsToBank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id', 'id');
    }
}
