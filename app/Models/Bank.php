<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bank extends Model
{
    use HasFactory;

    protected $table = 'bank';

    protected $fillable = [
       'user_id', 'name', 'address'
    ];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function bankBelongsToUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function manyBankExchangeRate(): HasMany
    {
        return $this->hasMany(BankExchangeRate::class,'bank_id');
    }
}
