<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBankExchangeRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'currency' => 'required',
            'amount' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'currency.required' => 'Wypełnij to pole :attribute',
            'amount.required' => 'Wypełnij to pole :attribute',
        ];
    }
}
