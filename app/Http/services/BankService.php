<?php

namespace App\Http\services;

use App\Models\Bank;

class BankService
{
    private $bankService;

    public function __construct(Bank $bank)
    {
        $this->bankService = $bank;
    }

    public function getBanksListings()
    {
        return $this->bankService->get();
    }

    public function getBankDetail(int $id)
    {
        return $this->bankService->findOrFail($id);
    }
}
