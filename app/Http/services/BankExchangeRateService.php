<?php

namespace App\Http\services;

use App\Models\BankExchangeRate;

class BankExchangeRateService
{
    private $bankExchangeRateService;

    public function __construct(BankExchangeRate $bankExchangeRate)
    {
        $this->bankExchangeRateService = $bankExchangeRate;
    }

    public function getBankExchangeRatesListings()
    {
        return $this->bankExchangeRateService->get();
    }

    public function getBankExchangeRateDetail(int $id)
    {
        return $this->bankExchangeRateService->findOrFail($id);
    }

    public function getBankExchangeRateByDate(string $date)
    {
        return $this->bankExchangeRateService->where('date', $date)->get();
    }

    public function getBankExchangeRateByCurrencyForDate(string $currency, string $date)
    {
        return $this->bankExchangeRateService->where([
            'currency' => $currency,
            'date' => $date
        ])->get();
    }
}
