<?php

namespace App\Http\Controllers\Controllers;

use App\Http\Controllers\Controller;
use App\Http\services\BankService;
use App\Models\Bank;
use App\Http\Requests\StoreBankRequest;
use App\Http\Requests\UpdateBankRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class BankController extends Controller
{
    private $bankServices;

    public function __construct(BankService $bankService)
    {
        $this->bankServices = $bankService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->bankServices->getBanksListings(), ResponseAlias::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBankRequest $request
     * @return JsonResponse
     */
    public function store(StoreBankRequest $request): JsonResponse
    {
        try {
            $bank = Bank::create([
                'name' => $request->name,
                'address' => $request->address,
                'user_id' => auth()->id(),
            ]);

            return \response()->json("Rekord Dodano {$bank->name}", ResponseAlias::HTTP_CREATED);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Bank $bank
     * @return JsonResponse
     */
    public function show(Bank $bank): JsonResponse
    {
        return \response()->json($this->bankServices->getBankDetail($bank->id), ResponseAlias::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBankRequest $request
     * @param Bank $bank
     * @return JsonResponse
     */
    public function update(UpdateBankRequest $request, Bank $bank): JsonResponse
    {
        try {
            $bank->fill([
                'id' => $bank->id,
                'name' => $request->name,
                'address' => $request->address,
                'user_id' => auth()->id(),
            ])->save();

            return \response()->json("Rekord zmieniono {$bank->name}", ResponseAlias::HTTP_CREATED);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bank $bank
     * @return JsonResponse
     */
    public function destroy(Bank $bank): JsonResponse
    {
        try {
            $bank->delete();

            return \response()->json("Rekord Usunięto", ResponseAlias::HTTP_NO_CONTENT);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }
}
