<?php

namespace App\Http\Controllers\Controllers;

use App\Http\Controllers\Controller;
use App\Http\services\BankExchangeRateService;
use App\Models\Bank;
use App\Models\BankExchangeRate;
use App\Http\Requests\StoreBankExchangeRateRequest;
use App\Http\Requests\UpdateBankExchangeRateRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class BankExchangeRateController extends Controller
{
    private $bankExchangeRateService;

    public function __construct(BankExchangeRateService $bankExchangeRateService)
    {
        $this->bankExchangeRateService = $bankExchangeRateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->bankExchangeRateService->getBankExchangeRatesListings(), ResponseAlias::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBankExchangeRateRequest $request
     * @return JsonResponse
     */
    public function store(StoreBankExchangeRateRequest $request): JsonResponse
    {
        try {
            $bank_id = $request->bank_id;

            if ($request->name) {
                if (isset(Bank::whereName($request->name)->first()->id)) {
                    $bank_id = Bank::whereName($request->name)->first()->id;
                }
            }

            $bankExchangeRate = BankExchangeRate::create([
                'currency' => $request->currency,
                'date' => $request->date,
                'amount' => $request->amount,
                'bank_id' => $bank_id,
            ]);

            return \response()->json("Rekord Dodano {$bankExchangeRate->currency} {$bankExchangeRate->date}", ResponseAlias::HTTP_CREATED);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param BankExchangeRate $bankExchangeRate
     * @return JsonResponse
     */
    public function show(BankExchangeRate $bankExchangeRate): JsonResponse
    {
        return \response()->json($this->bankExchangeRateService->getBankExchangeRateDetail($bankExchangeRate->id), ResponseAlias::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param string $date
     * @return JsonResponse
     */
    public function showByDate(string $date): JsonResponse
    {
        $result = $this->bankExchangeRateService->getBankExchangeRateByDate($date);

        return \response()->json($result, ResponseAlias::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param string $currency
     * @param string $date
     * @return JsonResponse
     */
    public function showByCurrencyWithDate(string $currency, string $date): JsonResponse
    {
        $result = $this->bankExchangeRateService->getBankExchangeRateByCurrencyForDate($currency, $date);

        return \response()->json($result, ResponseAlias::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBankExchangeRateRequest $request
     * @param BankExchangeRate $bankExchangeRate
     * @return JsonResponse
     */
    public function update(UpdateBankExchangeRateRequest $request, BankExchangeRate $bankExchangeRate): JsonResponse
    {
        try {
            $bank_id = $request->bank_id;

            if ($request->name) {
                if (isset(Bank::whereName($request->name)->first()->id)) {
                    $bank_id = Bank::whereName($request->name)->first()->id;
                }
            }

            $bankExchangeRate->fill([
                'id' => $bankExchangeRate->id,
                'currency' => $request->currency,
                'date' => $request->date,
                'amount' => $request->amount,
                'bank_id' => $bank_id,
            ])->save();

            return \response()->json("Rekord zmieniono {$bankExchangeRate->currency} {$bankExchangeRate->date}", ResponseAlias::HTTP_CREATED);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BankExchangeRate $bankExchangeRate
     * @return JsonResponse
     */
    public function destroy(BankExchangeRate $bankExchangeRate): JsonResponse
    {
        try {
            $bankExchangeRate->delete();

            return \response()->json("Rekord Usunięto", ResponseAlias::HTTP_NO_CONTENT);
        } catch (QueryException $e) {
            dd($e->errorInfo[2]);
        }
    }
}
