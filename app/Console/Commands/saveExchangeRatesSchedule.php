<?php

namespace App\Console\Commands;

use App\Models\Bank;
use App\Models\BankExchangeRate;
use Illuminate\Console\Command;

class saveExchangeRatesSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:startTimeExchangeRate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle(): void
    {
        BankExchangeRate::factory(1)->create()->each(function ($u) {
            BankExchangeRate::factory(Bank::class)->create(['bank_id' => rand(0,10)]);
        });

        $this->comment(BankExchangeRate::all());
    }
}
